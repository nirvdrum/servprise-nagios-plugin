#!/bin/sh

# Copyright 2007 - 2008 Servprise International, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


#################################
# Start of configuration items. #
#################################

# Fully specifed path to your Nagios plugins installation directory.
NAGIOS_PLUGIN_DIR='/usr/lib/nagios/plugins'

# Fully specified path to you Nagios configuration files directory.
# If your Nagios configuration does not load other configuration files
# by directory, then specify a temporary directory to store the WebReboot
# Nagios plugin configuration file.
NAGIOS_CONFIG_DIR='/etc/nagios-plugins/config'

# Username that the Nagios process will run as.  The plugin installation
# directory must be owned by this user.
NAGIOS_USER='nagios'

###############################
# End of configuration items. #
###############################



#################################################
#################################################
### DO NOT MODIFY ANY OF THE FOLLOWING LINES. ###
#################################################
#################################################

#########################################
# WebReboot Nagios Plugin installation. #
#########################################

check_error()
{
	if [ $? != 0 ]; then
		printf "          ********** FAILURE **********\n"
		
		if [ "$1" ]; then
			printf "          $1\n"
		fi
		
		exit -1
	fi
}

printf "Beginning WebReboot(R) Nagios(R) Plugin 1.1.1 installation.\n\n"

printf "Starting re-requisite dependencies check . . .\n\n"

python src/testenv.py
if [ $? != 0 ]; then
	printf "\nPre-requisite dependencies are missing.  Please install them and then try again.\n\n"
	
	exit -1
else
	printf "\nPre-requisite dependencies are properly installed.  Continuing installation.\n\n"
fi


# Install the plugins.
if [ -d $NAGIOS_PLUGIN_DIR/webreboot ]; then
	mv $NAGIOS_PLUGIN_DIR/webreboot/settings.py $NAGIOS_PLUGIN_DIR/webreboot/settings.py.bak
	check_error
	
	printf "Detected existing installation.  Copying settings file to '$NAGIOS_PLUGIN_DIR/webreboot/settings.py.bak'.\n\n"
else
	mkdir $NAGIOS_PLUGIN_DIR/webreboot
	check_error "Please verify that \$NAGIOS_PLUGIN_DIR is set correctly."
	
	printf "No previous installation detected.  Creating location '$NAGIOS_PLUGIN_DIR/webreboot'.\n\n"
fi

cp -a src/* $NAGIOS_PLUGIN_DIR/webreboot
check_error

printf "Copied Nagios plugin files to '$NAGIOS_PLUGIN_DIR/webreboot'.\n\n"

# Set up permissions.
chown -R $NAGIOS_USER $NAGIOS_PLUGIN_DIR/webreboot
check_error "Please verify that \$NAGIOS_USER is set correctly."

chgrp -R root $NAGIOS_PLUGIN_DIR/webreboot
check_error

chmod -R 700 $NAGIOS_PLUGIN_DIR/webreboot
check_error

printf "Set permissions on '$NAGIOS_PLUGIN_DIR/webreboot' to 700 for '$NAGIOS_USER:root'.\n\n"

# Install the command configuration.
cat webreboot.cfg | sed 's:NAGIOS_PLUGIN_DIR:'$NAGIOS_PLUGIN_DIR':g' > $NAGIOS_CONFIG_DIR/webreboot.cfg
check_error "Please verify that \$NAGIOS_CONFIG_DIR is set correctly."

printf "Copied command definition file to '$NAGIOS_CONFIG_DIR/webreboot.cfg'.\n\n"

printf "Congratulations.  Installation is now complete.  You may now configure the plugin by editing '$NAGIOS_PLUGIN_DIR/webreboot/settings.py'.\n"
