# Copyright 2007 - 2008 Servprise International, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from wr_3_0.WebReboot import WebReboot as WR3
from wre_1_2.WebReboot import WebReboot as WRE
import logging, os, threading, time

# Try to load the Python SQLite binding.  On Python 2.5, this will be named "sqlite3".
# On prior versions of Python, it will be named "pysqlite2".
try:
    import sqlite3
except ImportError:
    try:
        from pysqlite2 import dbapi2 as sqlite3
    except ImportError, e:
        logging.error('Failed to load "pysqlite2" module.  Please make sure it is on your system path.')
        
        raise e

# The indent is used to show nested calls in the log_timing decorator.  It is a bit odd,
# however, because the logging for the nested function calls are made before the logging
# for the parent calls (necessary to show total execution time).  As a result, the output
# must be read in reverse (an inverted tree), but the items in a given level must be read
# top-to-bottom.
# Ex.
#     sub-2-statement-1
#     sub-2-statement-2
#     sub-2-statement-3
#  sub-1-statement-1
# outer-1
#
# Here, you can see how the statements in a given level increase in number and how each level
# increases in number.
indent = ''
def log_timing(func):
    def wrapper(*arg):
        global indent
        indent = indent + '  '
        
        t1 = time.time()
        res = func(*arg)
        t2 = time.time()
        
        indent = indent[:-2]
        logging.info('%s%s took %0.3f ms' % (indent, func.func_name, (t2-t1)*1000.0))
        
        return res
    return wrapper

class GetConnectedHostsThread(threading.Thread):
    '''Fetches all of the connected hosts for a given WebReboot.'''
    
    def __init__(self, webreboot):
        threading.Thread.__init__(self)
        
        self.webreboot = webreboot
        
        self.connected_hosts = []
    
    def run(self):
        self.connected_hosts = self.webreboot.get_connected_hosts()


class Cache(object):
    def __init__(self):
        
        # Construct the database path relative to the location of the Nagios module.  This helps us locate
        # the DB no matter where the plugin gets installed or is executed from.
        import nagios
        path = os.path.dirname(nagios.__file__)
        db = os.path.join(path, 'cache.db')
        
        self.conn = sqlite3.connect(db, isolation_level=None)
        
        self.webreboots = {}
    

    def create_db(self):
        '''Creates the SQLite database if it does not yet exist.'''
        
        cur = self.conn.cursor()
        
        # Create the 'webreboots' table if necessary.
        cur.execute('''SELECT name FROM SQLITE_MASTER WHERE type='table' AND name='webreboots' ''')
        if cur.fetchone() is None:
            logging.debug('Adding new "webreboots" table.')
            
            cur.execute('''CREATE TABLE webreboots(id INTEGER PRIMARY KEY, host TEXT UNIQUE NOT NULL, type TEXT NOT NULL)''')
        
        # Create the 'locations' table if necessary.
        cur.execute('''SELECT name FROM SQLITE_MASTER WHERE type='table' AND name='locations' ''')
        if cur.fetchone() is None:
            logging.debug('Adding new "locations" table.')
            
            cur.execute('''CREATE TABLE locations(id INTEGER PRIMARY KEY, host TEXT NOT NULL, webreboot_id INTEGER NOT NULL, port TEXT NOT NULL)''')
    
    @log_timing
    def add_webreboot(self, host, scheme, port, username, password, type):
        '''Adds a cache entry for a WebReboot and its type.'''
        
        # If we don't have one already, create the actual WebReboot object for the local map.
        # Unlike the database, this is reconstructed on every start-up.  It's how we avoid persisting
        # username and password in anything but the settings file.
        if host not in self.webreboots:
            if type == 'wre':
                self.webreboots[host] = WRE(host, scheme, port, username, password)
            else:
                self.webreboots[host] = WR3(host, scheme, port, username, password)
        
        # Cache the type of the WebReboot in the WebReboot itself.  This will save introspection calls later on.
        self.webreboots[host].type = type
        
        # Now check if the entry already exists in the database.       
        cached_type = self._get_webreboot_type(host)
        
        # Add the entry if it doesn't exist.
        if cached_type is None:
            try:
                cur = self.conn.cursor()
                cur.execute('''INSERT INTO webreboots(host, type) VALUES(?,?)''', (host, type))
                
            except Exception, e:
                raise Exception('Error adding WebReboot "%s" of type "%s" to cache.' % (host, type), e)
            
            # Given that this is the first time we've seen this WebReboot, update the entire cache.
            # Otherwise, the WebReboot will be ignored in lieu of hosts on other units (unless something
            # does cause a cache fault).
            self.update_hosts()
            
        # If an entry for the hostname does exist, check that we're not adding 
        elif cached_type != type:
            self.delete_webreboot(host)
            
            # Now that the old WebReboot entries have been purged, try to add this one back to the cache.
            return self.add_webreboot(host, type)
    
    @log_timing   
    def delete_webreboot(self, host):
        '''Deletes a WebReboot and all of its entries from the cache.'''
        
        # Remove the local mapped copy first.
        if host in self.webreboots:
            del self.webreboots[host]
        
        # Remove any cached host locations for the WebReboot.
        self.clear_hosts(host)
        
        webreboot_id = self._get_webreboot_id(host)
        
        # If the WebReboot doesn't exist, there's not much we can do.
        if webreboot_id is None:
            return
        
        # Now remove the WebReboot from the DB.
        cur = self.conn.cursor()
        cur.execute('''DELETE from webreboots where id=?''', (webreboot_id,))
    
    @log_timing
    def update_hosts(self):
        '''Updates the cache of hosts on all WebReboots.'''
        
        threads = []
        
        # Start fetching the connected hosts from each registered WebReboot in its own thread.
        # This yields substantial time savings over a serial method.
        for webreboot in self.webreboots.values():
            t = GetConnectedHostsThread(webreboot)
            threads.append(t)
            t.start()
        
        # Now that we've started all of the threads, wait until each one of them has finished,
        # and update the cache with the results of the thread operation.
        for thread in threads:
            thread.join()
            
            self.clear_hosts(thread.webreboot.webreboot_host)
            
            for (host, port) in thread.connected_hosts:
                self.record_host_location(host, thread.webreboot, port)
    
    @log_timing
    def search_for_host(self, host):
        '''Searches the DB for the given host and returns the WebReboot and port it is on if it exists.
        Returns None if no cached value is found.'''
        
        cur = self.conn.cursor()
        
        # Take a first check of the cache.
        cur.execute('''SELECT webreboots.host, port FROM webreboots, locations WHERE locations.host=? AND locations.webreboot_id=webreboots.id''', (host,))
        
        # If we haven't seen anything in the DB yet, update the cache.
        rows = cur.fetchall()
        if len(rows) == 0:
            self.update_hosts()
        
        # Now that the cache has been updated, try again.
        cur.execute('''SELECT webreboots.host, port FROM webreboots, locations WHERE locations.host=? AND locations.webreboot_id=webreboots.id''', (host,))
        
        # If we still haven't found anything in the DB, just return None to the caller.
        rows = cur.fetchall()
        if len(rows) == 0:
            return (None, None)
        
        # We've found a match at this point.  Now, let's make sure that it's fresh.
        else:
            # Check each row returned to see if any entry for the host is in a non-CONNECTED state or if the host name has changed..
            for row in rows:
                try:
                    webreboot_hostname = row[0]
                    webreboot = self.webreboots[webreboot_hostname]
                    
                except KeyError:
                    # If we don't have a WebReboot object cached for the given hostname, then this is an old cache entry and
                    # we should remove them.  Since this affects the remainder of the search process, we should start that over as well.
                    self.delete_webreboot(webreboot_hostname)
                    
                    return self.search_for_host(host)
                    
                port_number = row[1]
                
                # If the host is no longer connected, then purge the entry from the cache and try to find the new location of the host if it exists.
                webreboot_type = self._get_webreboot_type(webreboot_hostname)
                if webreboot_type is None:
                    raise Exception('Cache out-of-sync.  Cannot look up type for WebReboot "%s".' % webreboot.webreboot_host)
                
                webreboot_port = webreboot.get_webreboot_port(port_number)
                
                # On the WRE, check both the port name and the port connection state for consistency.
                if webreboot_type == 'wre':
                    if (webreboot_port._ConnectionStatus != 'CONNECTED') or (webreboot_port._Name != host):
                        self.delete_host(host)
                
                        return self.search_for_host(host)
                    
                # On the WR 3.0, all we can test is the port name consistency.
                elif webreboot_type == 'wr3':
                    if webreboot_port != host:
                        self.delete_host(host)
                
                        return self.search_for_host(host)
            
            # If we've made it here, then all entries for the given host are marked as CONNECTED and the names are consistent.
            # So, now do a duplicate check.
            if len(rows) > 1:
                msg = 'Duplicate entries for host "%s"' % host
                
                for row in rows:
                    msg += '\nFound on WebReboot "%s" on port "%s"' % (row[0], row[1])
                
                raise Exception(msg)
            
            # If we've made it here, there was a single consistent entry, so return it as the authorative answer.
            row = rows[0]
            webreboot = self.webreboots[row[0]]
            port_number = row[1]
            
            logging.debug('Host "%s" found on port "%s" of WebReboot "%s"' % (host, port_number, webreboot.webreboot_host))
            
            return (webreboot, port_number)
    
    @log_timing
    def record_host_location(self, host, webreboot, port):
        '''Records the location of the host.'''
        
        cur = self.conn.cursor()
        
        # See if we have an existing record we need to update or a new one to add.
        try:
            webreboot_id = self._get_webreboot_id(webreboot.webreboot_host)
            
            cur.execute('''INSERT INTO locations(host, webreboot_id, port) VALUES(?, ?, ?)''', (host, webreboot_id, port))
            
        except Exception, e:
            raise Exception('Failure inserting value into cache.', e)
        
    @log_timing
    def clear_hosts(self, host):
        '''Clears all host entries for the given WebReboot.'''
        
        webreboot_id = self._get_webreboot_id(host)
        
        # If the WebReboot doesn't exist, there's not much we can do.
        if webreboot_id is None:
            return
        
        # Now update the WebReboot table and the host location table.
        cur = self.conn.cursor()
        cur.execute('''DELETE from locations where webreboot_id=?''', (webreboot_id,))
    
    @log_timing
    def delete_host(self, host):
        '''Purges the host's locations from the cache.'''
        
        cur = self.conn.cursor()
        
        try:
            cur.execute('''DELETE FROM locations WHERE host=?''', (host,))
            
        except Exception, e:
            raise Exception('Failure deleting entry from cache.', e)
        
    def get_all_hosts(self):
        '''Utility method for fetching all of the host entries out of the DB.'''
        cur = self.conn.cursor()
        
        cur.execute('''SELECT * FROM locations''')
        
        return cur.fetchall()

    ##################################################################
    ###################### Utility methods. ##########################
    ##################################################################
        
    def _get_webreboot_type(self, host):
        '''Returns the type of the WebReboot instance.  Either 'wre' or 'wr3'.'''
        
        cur = self.conn.cursor()
        
        cur.execute('''SELECT type FROM webreboots where host=?''', (host,))
        row = cur.fetchone()
        
        # If the WebReboot doesn't exist in the DB, indicate so to the user.
        if row is None:
            return None
        
        # Return the type.
        return row[0]
    
    def _get_webreboot_id(self, host):
        # Remove the DB entries.
        cur = self.conn.cursor()
        
        cur.execute('''SELECT id from webreboots where host=?''', (host,))
        row = cur.fetchone()
        
        # If the WebReboot doesn't exist in the DB, indicate so to the user.
        if row is None:
            return None
        
        return row[0]