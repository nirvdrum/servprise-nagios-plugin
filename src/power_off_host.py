# Copyright 2007 - 2008 Servprise International, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from common import *
from nagios import *

def print_command_help():
    print 'power_off_host: Turns a host off.\n'
    
    print 'This event handler is designed to work with a service check.'
    print 'Hosts are only powered down under the following conditions:'
    print '    1) The service check is in a "CRITICAL" state and'
    print '        a) The service state type is "SOFT" and the the service check has failed 2 times.'
    print '        b) The service state type is "HARD".'
    print ' If you would like to customize these conditions, you will have to modify the power_off_host.py file.'
    
    print '\nValid usage: nagios.py -x power_off_host -H <hostname> --state=<state> --statetype=<state_type> --attempt=<attempt>'
    print '    hostname: The logical name of the host to operate on.'
    print '    state: Indicates whether the service is "OK", "WARNING", "CRITICAL", or "UNKNOWN".'
    print '    state_type: Indicates whether the state type is "HARD" or "SOFT".'
    print '    attempt: The number of times the service check has failed.'

def check_missing_args(params):
    check_missing_arg('hostname', '-H', params.host)
    check_missing_arg('state', '--state', params.state)
    check_missing_arg('state_type', '--statetype', params.state_type)
    check_missing_arg('attempt', '--attempt', params.attempt)

def execute_wre(webreboot, params):
    logging.info('Attempting to turn off host "%s".' % params.host)
    
    # We really only need to do something if the host is powered on,
    # and the service is in a critical state.
    if 'CRITICAL' == params.state and ('2' == params.attempt or 'HARD' == params.state_type):   
        # Let's make sure that the webreboot_host is actually off before we try to
        # turn it on.  This is more of a safeguard than anything that should
        # happen in practice.
        if webreboot.is_power_on(params.port_number):
            webreboot.power_off(params.port_number)
        
            logging.info('Turning off host "%s".' % params.host)

        else:
            logging.warn('Attempted to turn host "%s" off, but it was already off.' % params.host)
            
    else:
        logging.info('Not turning off host "%s".  Conditions for host turn off not met.' % params.host)
        logging.info('Conditions are: Host state => "%s"; state type => "%s"; attempt count => "%s".' % (params.state, params.state_type, params.attempt))

    # Indicate success.
    return OK

def execute_wr3(webreboot, params):
    logging.info('Attempting to turn off host "%s".' % params.host)
    
    # We really only need to do something if the host is powered on,
    # and the service is in a critical state.
    if 'CRITICAL' == params.state and ('2' == params.attempt or 'HARD' == params.state_type):   
        from subprocess import Popen, PIPE

        # Fire off the command and wait until it completes.
        p = Popen(['java', '-cp', 'lib/webreboot_nagios.jar:lib/webreboot_api-1.2.jar', 'com.servprise.nagios.Nagios', webreboot.webreboot_host, webreboot.username, webreboot.password, webreboot.scheme, webreboot.port, 'power_off_host', params.host], stdout=PIPE)
        p.wait()

        # Output any error messages for Nagios.
        print p.stdout.read()
    
        return p.returncode

    # Indicate success.
    return OK