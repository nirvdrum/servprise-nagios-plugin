#!/bin/env python

# Copyright 2007 Servprise International, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from common import WebRebootException
from WebRebootService_services import *
from ZSI.client import AUTH

import httplib, socket, sys

def safe_exec(func):
    '''A decorator that wraps the function in a try...except block.
    The idea is to catch any sort of global Web Service failure in
    communicating with the WebReboot Enterprise.'''
    
    def decorated_function(*args):
        try:
            # Try to execute the function.
            return func(*args)
        
        # This normally happens if the user provides incorrect username and password information.
        except httplib.IncompleteRead, e:
            error = e[0][0]
            
            # See if it was an HTTP 401.
            if '401' in error:
                raise WebRebootException('Invalid authentication credentials supplied for WebReboot Enterprise.')
            
            else:
                # We're not sure what went wrong, so just raise an exception.
                raise WebRebootException('Error communicating with WebReboot Enterprise: %s' % e)
        
        # This normally happens if the user provides an incorrect IP address, port, or scheme.
        except socket.error, e:
            raise WebRebootException('Invalid WebReboot connection information supplied: %s' % e)
        
        # If we encounter a WebRebootException, it means we handled another exception deeper down the
        # stack.  We don't want to process it multiple times, so short-circuit things here and let the
        # global handler do what it needs to.
        except WebRebootException:
            raise
        
        # At this point, we're not sure what went wrong so just raise exception.
        except Exception, e:
            if e is None:
                raise WebRebootException('Unknown error occurred (empty exception): %s' % sys.exc_info()[0])
            else:
                raise WebRebootException('Unknown error occurred (non-empty exception): %s %s %s' % (e.__class__, e, sys.exc_info()[0]))

    
    return decorated_function


def safe_request(request):
    '''A decorator that wraps a Web Services request to ensure that we check the response
    in an appropriate manner.'''
    
    def decorated_function(*args):
        # Issue the request.
        res = request(*args)

        if res._CommandResult._Error:
            raise WebRebootException('WebReboot Enterprise request failed: %s' % res._CommandResult._ResultMessage)
        
        return res
        
    return decorated_function


class WebReboot(object):
    @safe_exec
    def __init__(self, host, scheme, port, username, password):
        
        if username != 'admin':
            raise WebRebootException('Multi-user for the WebReboot Enterprise is not yet supported.  Username must be "admin".')
        
        # Set up the Web service connection to the WRE.
        loc = WebRebootServiceLocator()
        self.port = loc.getWebReboot('%s://%s:%s/admin/services/WebRebootService' % (scheme, host, port), auth=(AUTH.httpbasic, username, password))

        # Store connection parameters.
        self.webreboot_host = host

        # Set up all the requests we may issue.
        self.port.DoPowerOff = safe_request(self.port.DoPowerOff)
        self.port.DoPowerOn = safe_request(self.port.DoPowerOn)
        self.port.DoPowerReboot = safe_request(self.port.DoPowerReboot)
        self.port.GetWebRebootPort = safe_request(self.port.GetWebRebootPort)
        self.port.GetBatchWebRebootPort = safe_request(self.port.GetBatchWebRebootPort)
    
    @safe_exec
    def get_connected_hosts(self):
        ret = []
        
        req = GetBatchWebRebootPortInput()
        res = self.port.GetBatchWebRebootPort(req)

        # Do a full scan of all ports on the WRE.
        for server in res._WebRebootPortArray._WebRebootPort:
            
            # We only care about connected ports.            
            if server._ConnectionStatus == 'CONNECTED':
                ret.append((server._Name, server._PortNumber))
        
        return ret
    
    
    @safe_exec
    def get_webreboot_port(self, port_number):
        req = GetWebRebootPortInput()
        req._PortNumber = int(port_number)
        
        res = self.port.GetWebRebootPort(req)
        
        return res._WebRebootPort
    
    @safe_exec
    def power_off(self, port_number):
        req = DoPowerOffInput()
        req._PortNumber = int(port_number)
        
        self.port.DoPowerOff(req)
    
    @safe_exec
    def power_on(self, port_number):
        req = DoPowerOnInput()
        req._PortNumber = int(port_number)
        
        self.port.DoPowerOn(req)

    @safe_exec
    def plain_reboot(self, port_number):
        req = DoResetSwitchRebootInput()
        req._PortNumber = int(port_number)
        
        # Make sure we can do a plain reboot before we attempt the operation.  The WRE does not report
        # a failure if the operation cannot be performed.
        webreboot_port = self.get_webreboot_port(port_number)
        if webreboot_port._ServerResetSwitchSupported:
            self.port.DoResetSwitchReboot(req)
        else:
            raise WebRebootException('Reset switch reboot method not supported for host: "%s".' % webreboot_port._Name)
    
    @safe_exec
    def power_reboot(self, port_number):
        req = DoPowerRebootInput()
        req._PortNumber = int(port_number)
        
        # Make sure we can do a power reboot before we attempt the operation.  The WRE does not report
        # a failure if the operation cannot be performed.
        webreboot_port = self.get_webreboot_port(port_number)
        if webreboot_port._ServerPowerSwitchSupported or webreboot_port._AcPowerSupported:
            self.port.DoPowerReboot(req)
        else:
            raise WebRebootException('Power reboot method not supported for host: "%s".' % webreboot_port.Name)
        
    @safe_exec
    def is_power_on(self, port_number):
        webreboot_port = self.get_webreboot_port(port_number)
        
        return 'ON' == webreboot_port._PowerStatus
    
    @safe_exec
    def get_temperature(self, port_number):
        webreboot_port = self.get_webreboot_port(port_number)
        
        return webreboot_port._TemperatureInCelsius