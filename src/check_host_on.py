# Copyright 2007 - 2008 Servprise International, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from common import *
from nagios import *

def print_command_help():
    print 'check_host_on: WebReboot Enterprise-only command for checking the power state of a host.'
    
    print '\nValid usage: nagios.py -x check_host_on -H <hostname>'
    print '    hostname: The logical name of the host to operate on.'

def check_missing_args(params):
    check_missing_arg('hostname', '-H', params.host)

def execute_wre(webreboot, params):    
    power_status = webreboot.is_power_on(params.port_number)
    
    if power_status:
        msg = 'HOST STATUS: Host "%s" is on.' % params.host
        ret = OK
        
    else:
        msg = 'HOST STATUS: Host "%s" is off.' % params.host     
        ret = FAILURE
    
    logging.info(msg)
        
    # Print out the message for Nagios's benefit.
    print msg
    
    return ret