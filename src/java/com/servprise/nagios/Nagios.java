/*
 * Copyright 2007 Servprise International, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.servprise.nagios;

import com.servprise.webreboot.WebReboot;
import com.servprise.webreboot.WebRebootException;

import java.util.List;
import java.util.Iterator;

public class Nagios
{
	private static final String GET_WEBREBOOT_PORT_CMD = "get_webreboot_port";
	private static final String GET_HOSTS_CMD = "get_hosts";
    private static final String POWER_OFF_CMD = "power_off_host";
    private static final String POWER_ON_CMD = "power_on_host";
    private static final String REBOOT_HOST_CMD = "reboot_host";

    public static void main(final String[] args)
    {
        // WebReboot connection parameters.
        final String webRebootHostname = args[0];
        final String username = args[1];
        final String password = args[2];
        final String scheme = args[3];
        final String port = args[4];

        // Nagios plugin parameters.
        final String command = args[5];
        final String commandArgument;
        if (args.length == 7)
        {
        	commandArgument = args[6];
        }
        else
        {
        	commandArgument = null;
        }

        // Handle debugging.  Ideally this would be done via log4j, but there's a bit of difficulty
        // in reconciling with the Python logging used in the rest of the Nagios plugin.  So, for the
        // time being (eternity?), we'll toggle debugging with the "-Ddebug" java command-line option
        // and print debug info to the standard output stream.
        boolean inDebugMode = false;
        final String debugProperty = System.getProperty("debug");
        
        if (null != debugProperty)
        {
            inDebugMode = true;
        }

        if (inDebugMode)
        {
            System.out.println("WebReboot hostname: " + webRebootHostname);
            System.out.println("Username: " + username);
            System.out.println("Password: " + password);
            System.out.println("Scheme: " + scheme);
            System.out.println("Port: " + port);
            System.out.println("Command: " + command);
            System.out.println("Command argument: " + commandArgument);
        }


        final WebReboot wr = new WebReboot();

        try
        {
            wr.connect(webRebootHostname, port, scheme);
        }
        catch (final WebRebootException e)
        {
            System.out.println("Could not connect to WebReboot 3.0:" + e.getMessage());

            System.exit(-1);
        }

        if (wr.isInUse())
        {
            System.out.println("Could not connect to WebReboot 3.0: Someone already logged in.");

            System.exit(-1);
        }


        // Now that we've established a connection to the WebReboot and verified that no one else is logged in,
        // log into the WebReboot and execute the Nagios command.
        int status = 0;
        try
        {
            wr.login(username, password);

            if (GET_WEBREBOOT_PORT_CMD.equals(command))
            {
            	final String serverID = commandArgument;
                
                // We communicate with the Nagios process via STDOUT, so this is how we'll send the server list.
                System.out.print(wr.getServerName(serverID));
            }
            
            else if (GET_HOSTS_CMD.equals(command))
            {
            	final StringBuffer output = new StringBuffer();
            	
            	final List serverIDs = wr.getServerIDs(wr.getUserID());
                for (final Iterator it = serverIDs.iterator(); it.hasNext();)
                {
                    final String serverID = (String) it.next();
                    final String serverName = wr.getServerName(serverID);

                    // Comma separate the server ID and the server name.
                    output.append(serverID).append(",").append(serverName);
                    
                    // Newline separate each entry because it's about the only character we can guarantee will
                    // not appear in the server name.
                    if (it.hasNext())
                    {
                    	output.append("\n");
                    }
                }
                
                // We communicate with the Nagios process via STDOUT, so this is how we'll send the server list.
                System.out.print(output);
            }
            
            else
            {
            	final String hostname = commandArgument;
            	final String serverID = getServerID(wr, hostname);

            	if (REBOOT_HOST_CMD.equals(command))
            	{
            		// Try a plain reboot first and fallback to a power reboot if necessary.
            		if (wr.isPlainRebootable(serverID))
            		{
            			wr.plainReboot(serverID);
            		}
            		else
            		{
            			wr.powerReboot(serverID);
            		}
            	}

            	else if (POWER_OFF_CMD.equals(command))
            	{
            		if (wr.isPowerRebootable(serverID))
            		{
            			wr.powerOff(serverID);
            		}
            		else
            		{
            			throw new UnsupportedOperationException("Hostname " + hostname + " is not configured for power reboot for user " + username + ".");
            		}
            	}

            	else if (POWER_ON_CMD.equals(command))
            	{
            		if (wr.isPowerRebootable(serverID))
            		{
            			wr.powerOn(serverID);
            		}
            		else
            		{
            			throw new UnsupportedOperationException("Hostname " + hostname + " is not configured for power reboot for user " + username + ".");
            		}
            	}

            	else
            	{
            		System.out.println("Problem interacting with WebReboot 3.0: Unsupported command: " + command);

            		status = -1;
            	}
            }
        }
        
        catch (final Exception e)
        {
            System.out.println("Problem interacting with WebReboot 3.0: " + e.getMessage());

            status = -1;
        }

        finally
        {
            // Make sure we always log out.
            wr.logout();
        }

        // Return the appropriate status code for the plugin.
        System.exit(status);
    }

    /**
     * Grabs the serverID for the given hostname.
     *
     * @param wr The WebReboot to search on.
     * @param hostname The hostname to search for.
     *
     * @return The serverID of the hostname, which is necessary for all API server actions.
     */
    private static String getServerID(final WebReboot wr, final String hostname)
    {
        String ret = null;

        // Grab all the server IDs off the WebReboot.  For each one, grab the associated server name and see
        // if it matches the supplied hostname.
        final List serverIDs = wr.getServerIDs(wr.getUserID());
        for (final Iterator it = serverIDs.iterator(); it.hasNext();)
        {
            final String serverID = (String) it.next();
            final String serverName = wr.getServerName(serverID);

            if (serverName.equals(hostname))
            {
                // We have to make sure we have an unambiguous match.  Otherwise, we could be working with the wrong host.
                if (null == ret)
                {
                    ret = serverID;
                }
                else
                {
                    throw new IllegalStateException("Hostname " + hostname + " is used by multiple ports.");
                }
            }
        }

        // Check that the hostname matches one of the servers connected to the WebReboot.  If there is no match,
        // there's not much we can do.
        if (null == ret)
        {
            throw new IllegalStateException("Hostname " + hostname + " not found on WebReboot 3.0.");
        }

        return ret;
    }
}
