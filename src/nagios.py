#!/usr/bin/python

# Copyright 2007 - 2008 Servprise International, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import getopt, logging, re, sys, traceback, urllib2
import cache
from logging.handlers import SysLogHandler
from urllib2 import HTTPError
from common import *

# Default settings.
WEBREBOOT_SCHEME = 'http'

COMMANDS = ('check_host_on', 'check_host_temperature', 'power_off_host', 'power_on_host', 'reboot_host')

# Load in the user's configured settings.
import settings
from settings import *
        
############################################
########### Utility functions.##############
############################################

def print_revision():
    print 'WebReboot(R) Plugin for Nagios(R) -- Version 1.1.1\n'
    
def print_usage():
    print '''Valid usage is: nagios.py -x <subcommand> [options] [args]
    Type 'nagios.py help <subcommand>' for help on a specific command.'''
    
    print '\nAvailable subcommands are:'
    for command in COMMANDS:
         print '    %s' % command
    
def print_help(command=None):
    print_revision()
    
    try:
        module = __import__(command)
        plugin = getattr(module, 'print_command_help')
    
        plugin()
    except:
        print_usage()
        
    sys.exit(0)
        
def check_missing_setting(name, value):
    if value is None or len(value) == 0:
        message = 'Missing required value: %s.  Please check your settings.py file.' % name
        
        print '\n' + message + '\n'
        logging.debug(message)
        
        sys.exit(-1)
        
def is_webreboot_enterprise(host, scheme, port):
    '''Indicates whether or not we are connecting to a WebReboot Enterprise.  If not, it can be assumed
    that we are connecting to a WebReboot 3.0 because they are the only two classes of WebReboots.'''
    
    # Try to access a URL that will only exist on a WebReboot 3.0.  If we can access it, we're on
    # a WebReboot 3.0.  Otherwise, we'll assume we're on a WebReboot Enterprise.  Yes, this logic
    # breaks down if the host simply is not up.  In that case, we propagate the exception up the stack.
    try:
        urllib2.urlopen('%s://%s:%s/params.htm' % (scheme, host, port))
        
    except HTTPError:
        return True
    
    except Exception, e:
        raise Exception('Unable to connect to WebReboot for version detection.', e)
    
    else:
        return False

def handle_exception(explanation, exception):
    '''A global exception handling utility.  This takes care of any common logging and program
    return status values.'''

    logging.error(explanation)
    logging.debug(traceback.format_exc())
        
    # Print the exception so that Nagios can display the first line as the Nagios status.
    print explanation

    return FAILURE

###########################################
######## Main program entry point. ########
###########################################

if __name__ == '__main__':
    # Set up some local variables.
    command = None
    params = Params()
    verbosity = int(LOG_VERBOSITY)
    
    # See if this is a help request first.
    if len(sys.argv) == 3 and sys.argv[1] == 'help':
        print_help(sys.argv[2])
        
    # If the user didn't supply any arguments, print the help message.
    elif len(sys.argv) == 1:
        print_help()
        
    else:
        # Try to parse any supplied command-line arguments.
        try:
            (opts, args) = getopt.getopt(sys.argv[1:], 'x:H:w:c:VFvh', ['help', 'execute=', 'hostname=', 'version', 'verbose=', 'state=', 'statetype=', 'attempt=', 'warning=', 'critical=', 'fahrenheit'])
        
        except getopt.GetoptError, e:
            print 'Problem parsing options: %s' % e
        
            print_usage()
            sys.exit(-1)

    # Now that the arguments have been parsed, setup whatever state they alter.
    for (option, value) in opts:
        if option in ('-x', '--execute'):
            command = value
        
        elif option in ('-h', '--help'):
            print_help()
        
        elif option in ('-H', '--hostname'):
            params.host = value
            
        elif option in ('-V', '--version'):
            print_revision()
            sys.exit(0)
            
        elif option == '-v':
            verbosity += 1
            
        elif option == '--verbose':
            verbosity = int(value)         
            
        elif option == '--state':
            params.state = value
            
        elif option == '--statetype':
            params.state_type = value
            
        elif option == '--attempt':
            params.attempt = value
            
        elif option in ('-w', '--warning'):
            params.warning_threshold = float(value)
            
        elif option in ('-c', '--critical'):
            params.critical_threshold = float(value)
            
        elif option in ('-F', '--fahrenheit'):
            params.fahrenheit = True
            
    # Validate the verbosity level.
    if verbosity > 3:
        print 'Verbosity level cannot exceed 3.'
                
        sys.exit(FAILURE)
            
    # Make sure all the required user-supplied parameters are provided.
    check_missing_setting('WEBREBOOTS', WEBREBOOTS)

    # These values come from the command definitions and thus, should never be None.
    # Nevertheless, we should be thorough in our checks.
    try:
        check_missing_arg('command', '-x', command)
        check_missing_arg('hostname', '-H', params.host)
    except WebRebootException, e:
        handle_exception(e.message, e)
        
        sys.exit(-1)
        

    # Set up the logger.
    if verbosity == 0:
        log_level = logging.ERROR

    elif verbosity == 1:
        log_level = logging.WARNING
        
    elif verbosity == 2:
        log_level = logging.INFO
        
    elif verbosity == 3:
        log_level = logging.DEBUG
    
    try:
        if USE_SYSLOG.lower() == 'true':
            try:
                handler = SysLogHandler((SYSLOG_HOST, int(SYSLOG_PORT)), getattr(SysLogHandler, 'LOG_%s' % SYSLOG_FACILITY))
            except AttributeError, e:
                raise Exception('The specified log facility of "%s" is invalid.  Please check your settings.py file.' % SYSLOG_FACILITY, e)
                
        else:
            handler = logging.FileHandler(LOG_FILE)
        
        logger = logging.getLogger()
        logger.setLevel(log_level)
        handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(message)s'))
        logger.addHandler(handler)
        
    except Exception, e:
        print 'Could not configure logger: %s' % e
        
        sys.exit(FAILURE)
        
        
    # Dispatch to the proper plugin.
    try:
        cache = cache.Cache()    
        cache.create_db()
        
        expression = r'(?P<scheme>\w+)://(?P<username>\w+):(?P<password>\w+)@(?P<host>[\w.]+)(:(?P<port>\d+))?'
        pattern = re.compile(expression)
        
        for url in WEBREBOOTS:
            match = pattern.match(url)
            
            scheme = match.group('scheme')
            username = match.group('username')
            password = match.group('password')
            host = match.group('host')
            port = match.group('port')
            
            # The default value for WEBREBOOT_PORT depends on the scheme being used.  As such, it must
            # be set conditionally and after we've loaded the user's settings.
            if port is None:
                if 'https'.lower() == scheme:
                    port = '443'
                else:
                    port = '80'
        
            try:
                # Discover what type of WebReboot we are running on.
                if is_webreboot_enterprise(host, scheme, port):
                    webreboot_type = 'wre'
                else:
                    webreboot_type = 'wr3'
            
            except Exception:
                logging.error('Could not connect to WebReboot "%s".  Please check your configuration for accuracy and verify the WebReboot is connected.' % host)
            
            else:
                # If we didn't have any problems connecting to the WebReboot, add it to the cache.
                cache.add_webreboot(host, scheme, port, username, password, webreboot_type)
        
        (webreboot, params.port_number) = cache.search_for_host(params.host)
        if params.port_number is None:
            raise Exception('Host "%s" was not found in a connected state on any WebReboot.' % params.host)
        
        
        # Dynamically load the plugin to execute.  The command passed as an argument corresponds
        # to a module name.  That module should have an 'execute' function.
        module = __import__(command)
        
        # Verify that all of the plugins parameters have been properly supplied.
        check_missing_args = getattr(module, 'check_missing_args')
        check_missing_args(params)
        
        # Execute the plugin and get the resultant status code.
        plugin = getattr(module, 'execute_%s' % webreboot.type)
        status = plugin(webreboot, params)

    # If we fail to load either the module or the function in the module, then either there's a typo
    # in the command name or the command is unsupported for the WebReboot type.  The former is a special
    # case of the latter, so we treat everything as the latter and inform the user appropriately.
    except (AttributeError, ImportError), e:
        if 'wre' == webreboot_type:
            name = 'WebReboot Enterprise'
        else:
            name = 'WebReboot 3.0'
        
        status = handle_exception('Unsupported command "%s" on %s.' % (command, name), e)

    except Exception, e:
        status = handle_exception('Exception executing plugin for command "%s": %s' % (command, e), e)
    
    # Now that we're exiting, clean up any resources.
    logging.shutdown()
    
    # Return the status code for Nagios.
    sys.exit(status)