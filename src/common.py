# Copyright 2008 Servprise International, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Nagios status codes.
OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3
FAILURE = -1
WR3_FAILURE = 255

############################################
############ Utility classes.###############
############################################

class Params(object):
    '''Represents parameters supplied to the script by Nagios.'''
    
    def __init__(self):
        self.host = None
        self.state = None
        self.state_type = None
        self.attempt = None
        
        self.critical_threshold = None
        self.warning_threshold = None
        
        self.fahrenheit = False

class WebRebootException(Exception):
    def __init__(self, exception):
        super(Exception, self).__init__(exception)
        
        self.exception = exception

############################################
########### Utility functions.##############
############################################

def check_missing_arg(name, flag, value):
    if value is None or len(value) == 0:
        raise WebRebootException('Missing required value: %s (%s).' % (name, flag))