# Copyright 2007 - 2008 Servprise International, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from common import *
from nagios import *

def print_command_help():
    print 'check_host_temperature: WebReboot Enterprise-only command for checking the temperature of a host.'
    
    print '\nValid usage: nagios.py -x check_host_temperature -H <hostname> [-w <warning_threshold>] [-c <critical_threshold>] [-F]'
    print '    hostname: The logical name of the host to operate on.'
    print '    warning_threshold (optional): Temperature threshold that when exceeded will return a WARNING state for the check.'
    print '    host_state_type (optional): Temperature threshold that when exceeded will return a CRITICAL state for the check.'
    print '    -F (optional): If present, all temperature units will be in Fahrenheit.  If omitted, Celcsius will be used instead.'

def check_missing_args(params):
    check_missing_arg('hostname', '-H', params.host)

def execute_wre(webreboot, params):    
    (temperature, unit) = convert_temperature_to_proper_unit(params.fahrenheit, webreboot.get_temperature(params.port_number))
    
    if params.critical_threshold is not None and temperature > params.critical_threshold:
        msg = 'CRITICAL - Temperature %.1f%s exceeds threshold of %s%s.' % (temperature, unit, params.critical_threshold, unit)
    
        ret = CRITICAL
    
    elif params.warning_threshold is not None and temperature > params.warning_threshold:
        msg = 'WARNING - Temperature %.1f%s exceeds threshold of %s%s.' % (temperature, unit, params.warning_threshold, unit)
    
        ret = WARNING
        
    else:
        msg = 'TEMPERATURE OK: %.1f%s' % (temperature, unit)
        
        ret = OK
    
    logging.info(msg)
        
    # Print out the message for Nagios's benefit.
    print msg
    
    return ret

def celsius_to_fahrenheit(temperature):
    ret = ((9.0 / 5.0) * temperature) + 32
    
    return ret

def convert_temperature_to_proper_unit(temp_is_in_fahrenheit, temperature):    
    # Convert to Fahrenheit if appropriate.
    if temp_is_in_fahrenheit:
        return (celsius_to_fahrenheit(temperature), 'F')
    
    # If we're not in Fahrenheit, we're in Celsius, which is the default anyway.
    return (temperature, 'C')