# Copyright 2007 - 2008 Servprise International, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#######################################
####### WebReboot Configuration #######
#######################################

WEBREBOOTS = [
    'http://admin:password@192.168.50.30',
#    'http://admin:password@192.168.50.231'
]


#######################################
###### General logging settings. ######
#######################################

# Set the log verbosity level.  The value must be between 0 - 3.
# 0 = Log errors.
# 1 = Log errors and warnings.
# 2 = Log errors, warnings, and status info.
# 3 = Log errors, warnings, status info, and debug info.
LOG_VERBOSITY = 0

# If 'true', then use syslog.  If 'false', log to file.
# You will have to further configure the appropriate logging
# method in one of the following sections.
USE_SYSLOG = 'false'


#######################################
######## File logging settings. #######
#######################################

# (Optional).  Only necessary to configure if the USE_SYSLOG
# setting is set to 'false'.

# Specify where the plugin should write its log data to.
LOG_FILE = '/var/log/nagios2/webreboot.log'


#######################################
########## Syslog settings. ###########
#######################################

# (Optional).  Only necessary to configure if the USE_SYSLOG
# setting is set to 'true'.

# Configures the syslog facility used for logging messages.
# Available options are: ALERT, AUTH, AUTHPRIV, CRIT, CRON, DAEMON,
# DEBUG, EMERG, ERR, INFO, KERN, LOCAL0, LOCAL1, LOCAL2, LOCAL3,
# LOCAL4, LOCAL5, LOCAL6, LOCAL7, LPR, MAIL, NEWS, NOTICE, SYSLOG,
# USER, UUCP, WARNING.
SYSLOG_FACILITY = 'USER'

SYSLOG_HOST = 'localhost'
SYSLOG_PORT = 514