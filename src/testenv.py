#!/usr/bin/python

# Copyright 2008 Servprise International, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys

# Do the PySQLite check.
try:
    print 'Searching for pysqlite3 (included with Python 2.5+) . . .',
    import sqlite3
    print 'SUCCESS'
    
except ImportError:
    try:
        print 'FAILED'
        print 'Searching for pysqlite2 (third party install) . . .',
        from pysqlite2 import dbapi2 as sqlite3
        print 'SUCCESS'
        
    except ImportError:
        print 'FAILED'
        print '    - Failed to load pysqlite module.  Please visit http://www.pysqlite.org/ for installation instructions.'
        
        sys.exit(-1)
        
# Now do the ZSI check.
try:
    print 'Searching for ZSI 2.0+ installation . . .',
    
    from ZSI.version import Version
    
    if Version[0] >= 2:
        print 'SUCCESS'
    
    else:
        print 'FAILED'
        print '     - You have version %s installed.  You must have version 2.0+ installed.' % ".".join(map(str, Version))
        print '     - Please visit http://pywebsvcs.sourceforge.net/ for installation instructions.  Do NOT install via easy_install!'
        
        sys.exit(-1)
    
except ImportError:
    print 'FAILED'
    print '     - Failed to load ZSI module.  Please visit http://pywebsvcs.sourceforge.net/ for installation instructions.  Do NOT install via easy_install!'
        
    sys.exit(-1)



# If all the checks passed, let's return a successful exit code.
sys.exit(0)