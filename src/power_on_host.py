# Copyright 2007 - 2008 Servprise International, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from common import *
from nagios import *

def print_command_help():
    print 'power_host_on: Powers a host on.\n'
    
    print 'This event handler is designed to work with a host check.'
    print 'Hosts are only powered on under the following conditions:'
    print '    1) The host is in a "DOWN" state and either:'
    print '        a) The host state type is "SOFT" and the the host check has failed 3 times.'
    print '        b) The host state type is "HARD".'
    print ' If you would like to customize these conditions, you will have to modify the power_on_host.py file.'
    
    print '\nValid usage: nagios.py -x power_on_host -H <hostname> --state=<state> --statetype=<state_type> --attempt=<attempt>'
    print '    hostname: The logical name of the host to operate on.'
    print '    state: Indicates whether the host is "UP", "DOWN", or "UNREACHABLE".'
    print '    state_type: Indicates whether the state type is "HARD" or "SOFT".'
    print '    attempt: The number of times the host check has failed.'

def check_missing_args(params):
    check_missing_arg('hostname', '-H', params.host)
    check_missing_arg('state', '--state', params.state)
    check_missing_arg('state_type', '--statetype', params.state_type)
    check_missing_arg('attempt', '--attempt', params.attempt)

def execute_wre(webreboot, params):
    logging.info('Attempting to turn on host "%s".' % params.host)
    
    # We really only need to do something if the host is powered down.
    # We'll wait until the third attempt just to make sure there was
    # no fluke.  We'll check at the 'HARD' state, too, which should only
    # happen if the host doesn't power back up before the max. number of
    # checks complete.
    if 'DOWN' == params.state and ('3' == params.attempt or 'HARD' == params.state_type):
        # Let's make sure that the webreboot_host is actually off before we try to
        # turn it on.  This is more of a safeguard than anything that should
        # happen in practice.
        if not webreboot.is_power_on(params.port_number):
            webreboot.power_on(params.port_number)
        
            logging.info('Turning on host "%s".' % params.host)

        else:
            logging.warn('Attempted to turn host "%s" on, but it was already on.' % params.host)
            
    else:
        logging.info('Not turning on host "%s".  Conditions for host turn on not met.' % params.host)
        logging.info('Conditions are: Host state => "%s"; state type => "%s"; attempt count => "%s".' % (params.state, params.state_type, params.attempt))

    # Indicate success.
    return OK

def execute_wr3(webreboot, params):
    logging.info('Attempting to turn on host "%s".' % params.host)
    
    # We really only need to do something if the host is powered down.
    # We'll wait until the third attempt just to make sure there was
    # no fluke.  We'll check at the 'HARD' state, too, which should only
    # happen if the host doesn't power back up before the max. number of
    # checks complete.
    if 'DOWN' == params.state and ('3' == params.attempt or 'HARD' == params.state_type):
        from subprocess import Popen, PIPE

        # Fire off the command and wait until it completes.
        p = Popen(['java', '-cp', 'lib/webreboot_nagios.jar:lib/webreboot_api-1.2.jar', 'com.servprise.nagios.Nagios', webreboot.webreboot_host, webreboot.username, webreboot.password, webreboot.scheme, webreboot.port, 'power_on_host', params.host], stdout=PIPE)
        p.wait()

        # Output any error messages for Nagios.
        print p.stdout.read()
    
        return p.returncode
            
    else:
        logging.info('Not turning on host "%s".  Conditions for host turn on not met.' % params.host)
        logging.info('Conditions are: Host state => "%s"; state type => "%s"; attempt count => "%s".' % (params.state, params.state_type, params.attempt))

    # Indicate success.
    return OK