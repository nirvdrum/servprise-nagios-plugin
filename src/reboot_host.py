# Copyright 2007 - 2008 Servprise International, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from common import *
from nagios import *

def print_command_help():
    print 'Reboot host: Reboots a host.\n'
    
    print 'This event handler is designed to work with a service check.'
    print 'Hosts are only rebooted under the following conditions:'
    print '    1) The service check is in a "CRITICAL" state and'
    print '        a) The service state type is "SOFT" and the the service check has failed 2 times.'
    print '        b) The service state type is "HARD".'
    print ' If you would like to customize these conditions, you will have to modify the reboot_host.py file.'
    
    print '\nValid usage: nagios.py -x reboot_host -H <hostname> --state=<state> --statetype=<state_type> --attempt=<attempt>'
    print '    hostname: The logical name of the host to operate on.'
    print '    state: Indicates whether the service is "OK", "WARNING", "CRITICAL", or "UNKNOWN".'
    print '    state_type: Indicates whether the state type is "HARD" or "SOFT".'
    print '    attempt: The number of times the service check has failed.'

def check_missing_args(params):
    check_missing_arg('hostname', '-H', params.host)
    check_missing_arg('state', '--state', params.state)
    check_missing_arg('state_type', '--statetype', params.state_type)
    check_missing_arg('attempt', '--attempt', params.attempt)

def execute_wre(webreboot, params):    
    logging.debug('Attempting to reboot host "%s".' % params.host)
    
    if 'CRITICAL' == params.state and ('2' == params.attempt or 'HARD' == params.state_type):
        # Try a plain reboot first and fall back to a power if the plain fails.  If the power
        # fails, too, then propagate the error up the stack.
        try:
            logging.info('Attempting to plain reboot host "%s".' % params.host)
            webreboot.plain_reboot(params.port_number)
            
        except WebRebootException, e:
            logging.info('Plain reboot of host "%s" failed: %s.  Attempting to power reboot host "%s".' % (params.host, e, params.host))
            webreboot.power_reboot(params.port_number)
            
    else:
        logging.info('Not rebooting host "%s".  Conditions for reboot not met.' % params.host)
        logging.info('Conditions are: Host state => "%s"; state type => "%s"; attempt count => "%s".' % (params.state, params.state_type, params.attempt))
    
    # Indicate success.
    return OK

def execute_wr3(webreboot, params):
    logging.debug('Attempting to reboot host "%s".' % params.host)
    
    if 'CRITICAL' == params.state and ('2' == params.attempt or 'HARD' == params.state_type):
       from subprocess import Popen, PIPE

       # Fire off the command and wait until it completes.
       p = Popen(['java', '-cp', 'lib/webreboot_nagios.jar:lib/webreboot_api-1.2.jar', 'com.servprise.nagios.Nagios', webreboot.webreboot_host, webreboot.username, webreboot.password, webreboot.scheme, webreboot.port, 'reboot_host', params.host], stdout=PIPE)
       p.wait()

       # Output any error messages for Nagios.
       print p.stdout.read()
    
       return p.returncode
   
    else:
        logging.info('Not rebooting host "%s".  Conditions for reboot not met.' % params.host)
        logging.info('Conditions are: Host state => "%s"; state type => "%s"; attempt count => "%s".' % (params.state, params.state_type, params.attempt))
    
    # Indicate success.
    return OK