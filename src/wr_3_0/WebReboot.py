#!/bin/env python

# Copyright 2007 Servprise International, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from common import WebRebootException, WR3_FAILURE
from subprocess import *

class WebReboot(object):
    def __init__(self, host, scheme, port, username, password):
        # Store connection parameters.
        self.webreboot_host = host
        self.scheme = scheme
        self.port = port
        self.username = username
        self.password = password
    
    def get_connected_hosts(self):
        '''Retrieves the list of server names and corresponding server IDs configured for the current user
        on the WebReboot.'''
        
        # Fire off the command and wait until it completes.
        p = Popen(['java', '-cp', 'lib/webreboot_nagios.jar:lib/webreboot_api-1.2.jar', 'com.servprise.nagios.Nagios', self.webreboot_host, self.username, self.password, self.scheme, self.port, 'get_hosts'], stdout=PIPE)
        p.wait()

        # Output any error messages for Nagios.
        output = p.stdout.read()
        
        if p.returncode == WR3_FAILURE:
            raise WebRebootException('Failed to retrieve server listing from WebReboot 3.0 ("%s"): %s' % (self.webreboot_host, output))
        
        ret = []
        
        # Parse the output from the WebReboot.
        # Server IDs and names are comma-separated, with each pair being newline separated.
        for line in output.split('\n'):
            (serverID, serverName) = line.split(',')
                
            ret.append((serverName, serverID))
    
        return ret
    
    def get_webreboot_port(self, server_id):
        '''Retrieves the hostname for the given server ID.'''
    
        # Fire off the command and wait until it completes.
        p = Popen(['java', '-cp', 'lib/webreboot_nagios.jar:lib/webreboot_api-1.2.jar', 'com.servprise.nagios.Nagios', self.webreboot_host, self.username, self.password, self.scheme, self.port, 'get_webreboot_port', server_id], stdout=PIPE)
        p.wait()

        # Output any error messages for Nagios.
        output = p.stdout.read()
        
        if p.returncode == WR3_FAILURE:
            raise WebRebootException('Failed to retrieve server listing from WebReboot 3.0 ("%s"): %s' % (self.webreboot_host, output))
        
        return output
        